﻿using Newtonsoft.Json;
using System;
using System.Net;
using ViaCEP.Models;

namespace ViaCEP
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Digite o CEP para buscar o endereço");
            string cep = Console.ReadLine();

            string url = "https://viacep.com.br/ws/" + cep + "/jason/";

            APIviaCEP aPIviaCEP = BuscarAPIviaCEP(url);

            Console.WriteLine("Endereço");
            Console.WriteLine(
                String.Format("CEP: {0} - Logradouro: {1} - Bairro: {2} - Localidade: {3} - UF:{4} - Unidade:{5} - Ibge:{6} - Gia:{7}  ", aPIviaCEP.CEP, aPIviaCEP.Logradouro, aPIviaCEP.Bairro, aPIviaCEP.Localidade, aPIviaCEP.UF, aPIviaCEP.Unidade, aPIviaCEP.IBGE, aPIviaCEP.Gia));

            Console.ReadLine();
            Console.WriteLine("\n\n");
            Console.ReadLine();
        }
        public static APIviaCEP BuscarAPIviaCEP(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);



            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            APIviaCEP aPIviaCEP = JsonConvert.DeserializeObject<APIviaCEP>(content);

            return aPIviaCEP;


        }
    }
}
