﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViaCEP.Models
{
    class APIviaCEP
    {

        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string Localidade { get; set; }
        public string Unidade { get; set; }
        public string IBGE { get; set; }
        public string Gia { get; set; }

    }



}